<?php
    include_once( 'views/header.php' );
    include_once( 'libs/editData.php' );
    include_once( 'libs/getData.php' );
?>

<h2 class="left"> Edit an Item</h2>
<span class="right">
    <a href="index.php" class="btn btn-success">Go Back</a>
	<a href="delete.php?id=<?php echo $_GET['id']; ?>&itemName=<?php echo $_GET['itemName']; ?>" class="btn btn-danger">
        Delete this item
    </a>
</span>
<div class="clear"></div>

<form class="well" action="edit.php?id=<?php echo $_GET['id']; ?>" method="post" enctype="multipart/form-data">
    <?php
        if ( $session_table_name == 'recipes') {
            include_once('views/editRecipe.php');
        }elseif ( $session_table_name == 'videos'){
            include_once('views/editVideo.php');
        }
    ?>
    <div class="form_elements">
        <input type="submit" name="edit_values" value="Edit Value" class="btn btn-primary"/>
    </div>


</form>
</div><!-- end container -->
</body>
</html>