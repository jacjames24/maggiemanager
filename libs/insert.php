<?php

	if(isset($_POST['insert_values']))
	{
		$param = $_POST;
		include_once( 'core/class.ManageDatabase.php' );
		$init = new ManageDatabase;



        if ( $session_table_name == 'recipes'){
            $fileName = $_FILES['imageName']['name'];
            $add = "uploads/images/".$fileName;

            $fields_name[] = 'name';
            $fields_name[] = 'description';
            $fields_name[] = 'isBreakfast';
            $fields_name[] = 'isLunch';
            $fields_name[] = 'isDinner';
            $fields_name[] = 'isSnack';
            $fields_name[] = 'servings';
            $fields_name[] = 'prepTime';
            $fields_name[] = 'cookingTime';
            $fields_name[] = 'countCalorie';
            $fields_name[] = 'countCarb';
            $fields_name[] = 'countProtein';
            $fields_name[] = 'countFat';
            $fields_name[] = 'nutritionSummary';
            $fields_name[] = 'isBeef';
            $fields_name[] = 'isChicken';
            $fields_name[] = 'isSeafood';
            $fields_name[] = 'isPasta';
            $fields_name[] = 'isVegetarian';
            $fields_name[] = 'isEgg';
            $fields_name[] = 'isPork';
            $fields_name[] = 'hasMagicSarap';
            $fields_name[] = 'hasSinigang';
            $fields_name[] = 'hasSavor';
            $fields_name[] = 'hasOysterSauce';
            $fields_name[] = 'imageName';
            $fields_name[] = 'ingredients';
            $fields_name[] = 'directions';
            $fields_name[] = 'smsSummary';
            $fields_name[] = 'maggiSiteURL';


            $field_value[] = trim($param['name']);
            $field_value[] = $param['description'];
            $field_value[] = ($param['isBreakfast'] == 1 ? 1 : 0);
            $field_value[] = ($param['isLunch'] == 1 ? 1 : 0);
            $field_value[] = ($param['isDinner'] == 1 ? 1 : 0);
            $field_value[] = ($param['isSnack'] == 1 ? 1 : 0);
            $field_value[] = $param['servings'];
            $field_value[] = $param['prepTime'];
            $field_value[] = $param['cookingTime'];
            $field_value[] = $param['countCalorie'];
            $field_value[] = $param['countCarb'];
            $field_value[] = $param['countProtein'];
            $field_value[] = $param['countFat'];
            $field_value[] = $param['nutritionSummary'];
            $field_value[] = ($param['isBeef'] == 1 ? 1 : 0);
            $field_value[] = ($param['isChicken'] == 1 ? 1 : 0);
            $field_value[] = ($param['isSeafood'] == 1 ? 1 : 0);
            $field_value[] = ($param['isPasta'] == 1 ? 1 : 0);
            $field_value[] = ($param['isVegetarian'] == 1 ? 1 : 0);
            $field_value[] = ($param['isEgg'] == 1 ? 1 : 0);
            $field_value[] = ($param['isPork'] == 1 ? 1 : 0);
            $field_value[] = ($param['hasMagicSarap'] == 1 ? 1 : 0);
            $field_value[] = ($param['hasSinigang'] == 1 ? 1 : 0);
            $field_value[] = ($param['hasSavor'] == 1 ? 1 : 0);
            $field_value[] = ($param['hasOysterSauce'] == 1 ? 1 : 0);
            $field_value[] = $fileName;
            $field_value[] = $param['ingredients'];
            $field_value[] = $param['directions'];
            $field_value[] = $param['smsSummary'];
            $field_value[] = $param['maggiSiteURL'];

            $field_count = count($fields_name);
            $fields_name = implode(',',$fields_name);

            for($x=1;$x<=$field_count; $x++){
                $total_counts[] = '?';
            }

            $bind_params = implode(',',$total_counts);

            $recipeID = $init->insertData($session_table_name,$fields_name,$bind_params,$field_value);

            if($recipeID > 0){
                $success = 'New recipe '.$param['recipeName'].' was added.';
            }else{
                $error = 'There was a slight problem';
            }

            if(move_uploaded_file($_FILES['imageName']['tmp_name'], $add)){

            }else{
                $error = $error. '<br/>There was a slight problem';
            }
        }elseif ( $session_table_name == 'videos'){
            $fileNameThumbnail = $_FILES['thumbnailName']['name'];
            $addThumbnail = "uploads/videos/thumbnails/".$fileNameThumbnail;

            $fileNameVideo = $_FILES['videoName']['name'];
            $addVideo = "uploads/videos/".$fileNameVideo;

            $fields_name[] = 'title';
            $fields_name[] = 'caption';
            $fields_name[] = 'thumbnailName';
            $fields_name[] = 'videoName';

            $field_value[] = $param['title'];
            $field_value[] = $param['caption'];
            $field_value[] = $fileNameThumbnail;
            $field_value[] = $fileNameVideo;

            $field_count = count($fields_name);
            $fields_name = implode(',',$fields_name);

            for($x=1;$x<=$field_count; $x++){
                $total_counts[] = '?';
            }

            $bind_params = implode(',',$total_counts);

            $videoId = $init->insertData($session_table_name,$fields_name,$bind_params,$field_value);

            if($videoId > 0){
                $success = 'New video '.$param['title'].' was added.';
            }else{
                $error = 'There was a slight problem';
            }

            if(move_uploaded_file($_FILES['thumbnailName']['tmp_name'], $addThumbnail)){

            }else{
                $error = $error. '<br/>There was a slight problem';
            }

            if(move_uploaded_file($_FILES['videoName']['tmp_name'], $addVideo)){

            }else{
                $error = $error. '<br/>There was a slight problem';
            }
        }


	}

?>