<?php

	if(isset($_POST['edit_values']))
	{
		include_once( 'session.php' );
	
		$param = $_POST;
		include_once( 'class.ManageDatabase.php' );
		$init = new ManageDatabase;
		$id = $_GET['id'];

        if ( $session_table_name == 'recipes') {
            $param['isBreakfast'] = ($param['isBreakfast'] == 1 ? 1 : 0);
            $param['isLunch'] = ($param['isLunch'] == 1 ? 1 : 0);
            $param['isDinner'] = ($param['isDinner'] == 1 ? 1 : 0);
            $param['isSnack'] = ($param['isSnack'] == 1 ? 1 : 0);

            $param['isBeef'] = ($param['isBeef'] == 1 ? 1 : 0);
            $param['isChicken'] = ($param['isChicken'] == 1 ? 1 : 0);
            $param['isSeafood'] = ($param['isSeafood'] == 1 ? 1 : 0);
            $param['isPasta'] = ($param['isPasta'] == 1 ? 1 : 0);
            $param['isVegetarian'] = ($param['isVegetarian'] == 1 ? 1 : 0);
            $param['isEgg'] = ($param['isEgg'] == 1 ? 1 : 0);
            $param['isPork'] = ($param['isPork'] == 1 ? 1 : 0);

            $param['hasMagicSarap'] = ($param['hasMagicSarap'] == 1 ? 1 : 0);
            $param['hasSinigang'] = ($param['hasSinigang'] == 1 ? 1 : 0);
            $param['hasSavor'] = ($param['hasSavor'] == 1 ? 1 : 0);
            $param['hasOysterSauce'] = ($param['hasOysterSauce'] == 1 ? 1 : 0);

            $fileName = $_FILES['imageName']['name'];
            $add = "uploads/images/" . $fileName;

            if ($fileName != "") {
                if (move_uploaded_file($_FILES['imageName']['tmp_name'], $add)) {
                    $param['imageName'] = $fileName;
                } else {

                }
            }

            $edit_values = $init->editData($session_table_name, $param, $id);
        }elseif ( $session_table_name == 'videos'){
            $fileNameThumbnail = $_FILES['thumbnailName']['name'];
            $addThumbnail = "uploads/videos/thumbnails/" . $fileNameThumbnail;


            if ($fileNameThumbnail != "") {
                if (move_uploaded_file($_FILES['thumbnailName']['tmp_name'], $addThumbnail)) {
                    $param['thumbnailName'] = $fileNameThumbnail;
                } else {

                }
            }

            $fileNameVideo = $_FILES['videoName']['name'];
            $addVideo = "uploads/videos/" . $fileNameVideo;

            if ($fileNameVideo != "") {
                if (move_uploaded_file($_FILES['videoName']['tmp_name'], $addVideo)) {
                    $param['videoName'] = $fileNameVideo;
                } else {

                }
            }

            $edit_values = $init->editData($session_table_name, $param, $id);
        }

	}
?>