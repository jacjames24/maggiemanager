<?php include_once( 'libs/listTables.php' ); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="bootstrap/css/jasny-bootstrap.min.css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="bootstrap/js/jasny-bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            var left = 1200
            $('#sms_counter').text('Characters left: ' + left);

            $('#smsSummary').keyup(function () {

                left = 1200 - $(this).val().length;
                $('#sms_counter').text('Characters left: ' + left);
            });
        });
    </script>
</head>
<body>	
	<div class="navbar">
	  <div class="navbar-inner">
		<div class="container">
			<a class="brand" href="index.php">
				Maggi Recipes Manager
			</a>
		</div>
	  </div>
	</div><!-- end navbar -->
 <div class="container">
	<div class="table_switcher">
		<form class="well form-inline" method="post" action="switch_tables.php?referrer=<?php echo $_SERVER['REQUEST_URI']; ?>">
			<label for="Table Name">Table Name</label>
            <select name="table_name">
                <option value="recipes" <?php if ( $session_table_name == 'recipes') echo " selected = 'selected' "?>>
                    Recipes
                </option>
                <option value="videos" <?php if ( $session_table_name == 'videos') echo " selected = 'selected' "?>>
                    Videos
                </option>
            </select>
			<input type="submit" name="switch_table" value="Switch Table" class="btn btn-primary"/>
		</form>
	</div><!-- end table_switcher -->
	
	<?php
		if(!isset($_SESSION['SESSION_TABLE_NAME'])){
            echo '<div class="alert alert-error"><p>Select a table to get started </p></div>';
			die;
		}
	?>
	
