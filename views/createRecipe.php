<div class="form_elements">
    <label for="Name">Name</label><input type="text" name="name" />
</div>

<div class="form_elements">
    <label for="Description">Description</label><textarea name="description" ></textarea>
</div>

<div class="fileinput fileinput-new" data-provides="fileinput">
    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
    <div>
                <span class="btn btn-default btn-file">
                    <span class="fileinput-new">Select image</span>
                    <span class="fileinput-exists">Change</span>
                    <input type="file" name="imageName">
                </span>
        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
    </div>
</div>

<div class="form_elements">
    <label for="Time of Meal">Time of Meal (Value can be more than 1)</label>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isBreakfast">Breakfast</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isLunch">Lunch</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isDinner">Dinner</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isSnack">Snack</label>
    </div>
</div>

<div class="form_elements">
    <label for="Time of Meal">Meal Category (Value can be more than 1)</label>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isBeef">Beef</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isChicken">Chicken</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isSeafood">Seafood</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isPasta">Pasta</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isVegetarian">Vegetarian</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isEgg">Egg</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isPork">Pork</label>
    </div>
</div>

<div class="form_elements">
    <label for="Time of Meal">Maggi Products in Recipe (Value can be more than 1)</label>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="hasMagicSarap">Magic Sarap</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="hasSinigang">Maggi Sinigang</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="hasSavor">Maggi Savor</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="hasOysterSauce">Oyster Sauce</label>
    </div>
</div>

<div class="form_elements">
    <label for="servings">Number of Servings (e.g. 4-6)</label><input type="text" name="servings" />
</div>

<div class="form_elements">
    <label for="prepTime">Preparation Time in Minutes (e.g. 30)</label><input type="text" name="prepTime" />
</div>

<div class="form_elements">
    <label for="cookingTime">Cooking Time in Minutes (e.g. 30)</label><input type="text" name="cookingTime" />
</div>

<div class="form_elements">
    <label for="cookingTime">Calorie Count</label><input type="text" name="countCalorie" />
</div>

<div class="form_elements">
    <label for="cookingTime">Carbohydrates Content</label><input type="text" name="countCarb" />
</div>

<div class="form_elements">
    <label for="cookingTime">Protein Content</label><input type="text" name="countProtein" />
</div>

<div class="form_elements">
    <label for="cookingTime">Fat Content</label><input type="text" name="countFat" />
</div>

<div class="form_elements">
    <label for="Description">Nutrition Summary</label><textarea name="nutritionSummary" ></textarea>
</div>

<div class="form_elements">
    <label for="Description">Ingredients (separate each ingredient via return/enter key)</label>
    <textarea name="ingredients" ></textarea>
</div>

<div class="form_elements">
    <label for="Description">Directions (separate each instruction via return/enter key)</label>
    <textarea name="directions" ></textarea>
</div>

<div class="form_elements">
    <label for="Description">SMS Summary (Content for the Share Recipe via SMS)</label>
    <textarea id="smsSummary" name="smsSummary" maxlength="1200"></textarea>
    <label id="sms_counter"></label>
</div>

<div class="form_elements">
    <label for="maggiSiteURL">URL of maggi web site (For sharing recipes via Facebook)</label>
    <input type="text" name="maggiSiteURL" />
</div>