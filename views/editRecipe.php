<div class="form_elements">
    <label for="Name">Name</label><input type="text" name="name" value="<?php echo $data['name']?>"/>
</div>

<div class="form_elements">
    <label for="Description">Description</label><textarea name="description" ><?php echo $data['description']?></textarea>
</div>

<div class="fileinput fileinput-new" data-provides="fileinput">
    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
        <img src="<?php echo "uploads/images/".$data['imageName']?>" />
    </div>
    <div>
        <span class="btn btn-default btn-file">
            <span class="fileinput-new">Select image</span>
            <span class="fileinput-exists">Change</span>
            <input type="file" name="imageName">
        </span>
        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
    </div>
</div>

<div class="form_elements">
    <label for="Time of Meal">Time of Meal (Value can be more than 1)</label>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isBreakfast" <?php echo ($data['isBreakfast']==1 ? 'checked' : '');?>>Breakfast</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isLunch" <?php echo ($data['isLunch']==1 ? 'checked' : '');?>>Lunch</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isDinner" <?php echo ($data['isDinner']==1 ? 'checked' : '');?>>Dinner</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isSnack" <?php echo ($data['isSnack']==1 ? 'checked' : '');?>>Snack</label>
    </div>
</div>

<div class="form_elements">
    <label for="Time of Meal">Meal Category (Value can be more than 1)</label>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isBeef" <?php echo ($data['isBeef']==1 ? 'checked' : '');?>>Beef</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isChicken" <?php echo ($data['isChicken']==1 ? 'checked' : '');?>>Chicken</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isSeafood" <?php echo ($data['isSeafood']==1 ? 'checked' : '');?>>Seafood</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isPasta" <?php echo ($data['isPasta']==1 ? 'checked' : '');?>>Pasta</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isVegetarian" <?php echo ($data['isVegetarian']==1 ? 'checked' : '');?>>Vegetarian</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isEgg" <?php echo ($data['isEgg']==1 ? 'checked' : '');?>>Egg</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="isPork" <?php echo ($data['isPork']==1 ? 'checked' : '');?>>Pork</label>
    </div>
</div>

<div class="form_elements">
    <label for="Time of Meal">Maggi Products in Recipe (Value can be more than 1)</label>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="hasMagicSarap" <?php echo ($data['hasMagicSarap']==1 ? 'checked' : '');?>>Magic Sarap</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="hasSinigang" <?php echo ($data['hasSinigang']==1 ? 'checked' : '');?>>Maggi Sinigang</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="hasSavor" <?php echo ($data['hasSavor']==1 ? 'checked' : '');?>>Maggi Savor</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" value="1" name="hasOysterSauce" <?php echo ($data['hasOysterSauce']==1 ? 'checked' : '');?>>Oyster Sauce</label>
    </div>
</div>

<div class="form_elements">
    <label for="servings">Number of Servings (e.g. 4-6)</label><input type="text" name="servings" value="<?php echo $data['servings']?>"/>
</div>

<div class="form_elements">
    <label for="prepTime">Preparation Time in Minutes (e.g. 30)</label><input type="text" name="prepTime" value="<?php echo $data['prepTime']?>"/>
</div>

<div class="form_elements">
    <label for="cookingTime">Cooking Time in Minutes (e.g. 30)</label><input type="text" name="cookingTime" value="<?php echo $data['cookingTime']?>"/>
</div>

<div class="form_elements">
    <label for="cookingTime">Calorie Count</label><input type="text" name="countCalorie" value="<?php echo $data['countCalorie']?>"/>
</div>

<div class="form_elements">
    <label for="cookingTime">Carbohydrates Content</label><input type="text" name="countCarb" value="<?php echo $data['countCarb']?>"/>
</div>

<div class="form_elements">
    <label for="cookingTime">Protein Content</label><input type="text" name="countProtein" value="<?php echo $data['countProtein']?>"/>
</div>

<div class="form_elements">
    <label for="cookingTime">Fat Content</label><input type="text" name="countFat" value="<?php echo $data['countFat']?>"/>
</div>

<div class="form_elements">
    <label for="Description">Nutrition Summary</label><textarea name="nutritionSummary" ><?php echo $data['nutritionSummary']?></textarea>
</div>

<div class="form_elements">
    <label for="Description">Ingredients (separate each ingredient via return/enter key)</label>
    <textarea name="ingredients" ><?php echo $data['ingredients']?></textarea>
</div>

<div class="form_elements">
    <label for="Description">Directions (separate each instruction via return/enter key)</label>
    <textarea name="directions" ><?php echo $data['directions']?></textarea>
</div>

<div class="form_elements">
    <label for="Description">SMS Summary (Content for the Share Recipe via SMS)</label>
    <textarea id="smsSummary" name="smsSummary" maxlength="1200"><?php echo $data['smsSummary']?></textarea>
    <label id="sms_counter"></label>
</div>

<div class="form_elements">
    <label for="maggiSiteURL">URL of maggi web site (For sharing recipes via Facebook)</label>
    <input type="text" name="maggiSiteURL" value="<?php echo $data['maggiSiteURL']?>"/>
</div>