<div class="form_elements">
    <label for="title">Title</label><input type="text" name="title" />
</div>

<div class="form_elements">
    <label for="Caption">Caption</label><textarea name="caption" ></textarea>
</div>

<div class="fileinput fileinput-new" data-provides="fileinput">
    <label for="thumbnailName">Thumbnail for the Video</label>
    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
    <div>
                <span class="btn btn-default btn-file">
                    <span class="fileinput-new">Select image</span>
                    <span class="fileinput-exists">Change</span>
                    <input type="file" name="thumbnailName">
                </span>
        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
    </div>
</div>

<div class="form_elements">
    <label for="Video">Video File</label><input type="file" name="videoName">
</div>
