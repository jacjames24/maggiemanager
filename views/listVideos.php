<thead>
<tr>
    <th>id</th>
    <th>Title</th>
    <th>Caption</th>
    <th>Video File Name</th>
    <th>Thumbnail File Name</th>
    <th>Actions</th>
</tr>
</thead>
<tbody>
<?php
    if($data !== 0) {
        foreach($data as $key => $value)
        {
            echo '<tr>';

            echo '<td>' . $value['id'] . '</td>';
            echo '<td>' . $value['title'] . '</td>';
            echo '<td>' . $value['caption'] . '</td>';
            echo '<td>' . $value['videoName'] . '</td>';
            echo '<td>' . $value['thumbnailName'] . '</td>';

            echo '<td>';
            echo       '<a href="edit.php?id='.$value['id'].'&itemName='.$value['title'].'"><i class="icon-edit"></i></a>
                        <a href="delete.php?id='.$value['id'].'&itemName='.$value['title'].'"><i class="icon-trash"></i></a>';
            echo '</td>';

            echo '</tr>';

        }
    }
?>
</tbody>