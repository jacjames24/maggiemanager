<thead>
<tr>
    <th>id</th>
    <th>Name</th>
    <th>Description</th>
    <th>Food Type</th>
    <th>Maggi Products</th>
    <th># of Views</th>
    <th>Actions</th>
</tr>
</thead>
<tbody>
<?php
    if($data !== 0) {
        foreach($data as $key => $value)
        {
            echo '<tr>';

            $foodType = array();

            if ($value['isBreakfast'] == 1){ array_push($foodType, 'Breakfast'); }
            if ($value['isLunch'] == 1){ array_push($foodType, 'Lunch'); }
            if ($value['isDinner'] == 1){ array_push($foodType, 'Dinner'); }
            if ($value['isSnack'] == 1){ array_push($foodType, 'Snack'); }

            $maggieProducts = array();

            if ($value['hasMagicSarap'] == 1){ array_push($maggieProducts, 'Magic Sarap'); }
            if ($value['hasSinigang'] == 1){ array_push($maggieProducts, 'Sinigang'); }
            if ($value['hasSavor'] == 1){ array_push($maggieProducts, 'Maggi Savor'); }
            if ($value['hasOysterSauce'] == 1){ array_push($maggieProducts, 'Oyster Sauce'); }

            echo '<td>' . $value['id'] . '</td>';
            echo '<td>' . $value['name'] . '</td>';
            echo '<td>' . $value['description'] . '</td>';
            echo '<td>' . implode($foodType, ", ") . '</td>';
            echo '<td>' . implode($maggieProducts, ", ") . '</td>';
            echo '<td>' . $value['numViews'] . '</td>';

            echo '<td>';
            echo '<a href="edit.php?id='.$value['id'].'&itemName='.$value['name'].'"><i class="icon-edit"></i></a>';
            echo '<a href="delete.php?id='.$value['id'].'&itemName='.$value['name'].'"><i class="icon-trash"></i></a>';
            echo '</td>';

            echo '</tr>';

        }
    }
?>
</tbody>