<?php
    include_once( 'views/header.php' );
    include_once( 'libs/insert.php' );
?>
<div class="container">
    <?php
        if(isset($success)) {
            echo '<div class="alert alert-success"><p>'.$success.'</p></div>';
        }elseif(isset($error)){
            echo '<div class="alert alert-error"><p>'.$error.'</p></div>';
        }
    ?>

    <h2 class="left"> Create New Item </h2>
    <span class="right"><a href="index.php" class="btn btn-success">Go Back</a></span>
    <div class="clear"></div>

    <form class="well" method="post" action="create.php" enctype="multipart/form-data">
        <?php
            if ( $session_table_name == 'recipes') {
                include_once('views/createRecipe.php');
            }elseif ( $session_table_name == 'videos'){
                include_once('views/createVideo.php');
            }
        ?>
        <div class="form_elements">
            <input type="submit" name="insert_values" value="Insert Value" class="btn btn-primary"/>
        </div>
    </form>
</div><!-- end container -->
</body>
</html>