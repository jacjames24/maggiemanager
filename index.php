<?php
    include_once( 'views/header.php' );
	include_once( 'libs/getData.php' );
?>
	<h2 class="left">Records from the Selected Table</h2>
	<span class="right"><a href="create.php" class="btn btn-success">Insert New Value</a></span>
 	<table class="table table-striped table-bordered table-condensed">
        <?php

            if ( $session_table_name == 'recipes') {
                include_once('views/listRecipes.php');
            } elseif ( $session_table_name == 'videos'){
                include_once('views/listVideos.php');
            }
        ?>
	</table>
 </div><!-- end container -->
 </body>
</html>