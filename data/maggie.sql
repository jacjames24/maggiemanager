-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Apr 07, 2015 at 05:52 PM
-- Server version: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `maggie`
--

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

DROP TABLE IF EXISTS `recipes`;
CREATE TABLE `recipes` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `imageName` varchar(500) DEFAULT NULL,
  `numViews` int(11) DEFAULT '0',
  `isBreakfast` tinyint(1) DEFAULT '0',
  `isLunch` tinyint(1) DEFAULT '0',
  `isDinner` tinyint(1) DEFAULT '0',
  `isSnack` tinyint(1) DEFAULT '0',
  `servings` varchar(10) DEFAULT '0',
  `prepTime` int(3) DEFAULT '0',
  `cookingTime` int(3) DEFAULT '0',
  `countCalorie` int(4) DEFAULT '0',
  `countCarb` int(4) DEFAULT '0',
  `countProtein` int(4) DEFAULT '0',
  `countFat` int(4) DEFAULT '0',
  `nutritionSummary` text,
  `isBeef` tinyint(1) DEFAULT '0',
  `isChicken` tinyint(1) DEFAULT '0',
  `isSeafood` tinyint(1) DEFAULT '0',
  `isPasta` tinyint(1) DEFAULT '0',
  `isVegetarian` tinyint(1) DEFAULT '0',
  `hasMagicSarap` tinyint(1) DEFAULT '0',
  `hasSinigang` tinyint(1) DEFAULT '0',
  `hasSavor` tinyint(1) DEFAULT '0',
  `hasOysterSauce` tinyint(1) DEFAULT '0',
  `ingredients` text,
  `directions` text
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `recipes`
--

TRUNCATE TABLE `recipes`;
--
-- Dumping data for table `recipes`
--

INSERT INTO `recipes` VALUES
(39, 'Adobong Sitaw', 'A staple side dish in Filipino cuisine, Adobong Sitaw is always a welcome addition to\r\nany dining table. It cooks quickly and fits any type of budget!', NULL, 0, 1, 1, 0, 0, '2-3', 20, 30, 100, 200, 102, 100, '', 1, 0, 0, 1, 0, 1, 0, 1, 0, 'Â½ kg squid, cleaned, reserve ink\r\n2 tbsp vegetable oil\r\n4 cloves garlic, minced\r\n1 pc medium onion, small diced\r\n2 pcs tomatoes, small diced\r\n1 pc finger chilli (siling panigang)\r\nÂ¼ cup vinegar\r\n1 pc bay leaf\r\nÂ¼ tsp freshly ground black pepper\r\n1 tbsp brown sugar\r\n1 sachet 8g MAGGI MAGIC SARAPÂ®', ''),
(47, '1', '23', 'Chap-Chae.png', 0, 1, 0, 0, 1, '4-8', 25, 24, 100, 15, 30, 15, 'Summary is a summary', 1, 1, 0, 0, 0, 1, 0, 0, 1, NULL, NULL),
(48, '1', '23', 'Chap-Chae.png', 0, 1, 0, 0, 1, '4-8', 25, 24, 100, 15, 30, 15, 'Summary is a summary', 1, 1, 0, 0, 0, 1, 0, 0, 1, NULL, NULL),
(49, '1', '23', 'Chap-Chae.png', 0, 1, 0, 0, 1, '4-8', 25, 24, 100, 15, 30, 15, 'Summary is a summary', 1, 1, 0, 0, 0, 1, 0, 0, 1, NULL, NULL),
(50, '1', '23', 'Chap-Chae.png', 0, 1, 0, 0, 1, '4-8', 25, 24, 100, 15, 30, 15, 'Summary is a summary', 1, 1, 0, 0, 0, 1, 0, 0, 1, NULL, NULL),
(51, '1', '23', 'Chap-Chae.png', 0, 1, 0, 0, 1, '4-8', 25, 24, 100, 15, 30, 15, 'Summary is a summary', 1, 1, 0, 0, 0, 1, 0, 0, 1, NULL, NULL),
(52, 'Cool Recipe', 'Cool Description', 'Chicken_Sate.png', 0, 1, 1, 0, 0, '2-3', 30, 40, 100, 59, 100, 100, '200', 1, 0, 0, 1, 0, 1, 0, 1, 0, NULL, NULL),
(54, '1', '1', 'Adobong-Pusit.png', 0, 1, 0, 0, 0, '1-2', 1, 1, 1, 1, 1, 1, '1', 1, 0, 0, 0, 0, 0, 1, 0, 0, NULL, NULL),
(56, 'Adobong Sitaw', 'A staple side dish in Filipino cuisine, Adobong Sitaw is always a welcome addition to any dining table. It cooks quickly and fits any type of budget!', 'Adobong-Sitaw.png', 0, 0, 1, 0, 0, '2-4', 20, 30, 150, 200, 25, 100, 'This is a cool summary', 0, 0, 1, 0, 0, 0, 1, 0, 0, '2 Tbsp	vegetable oil\r\n2 cloves	garlic, minced\r\n1/2 kg	sitaw, cut into 2 Â½ inches\r\n2 Tbsp	vinegar\r\n2 Tbsp	soy sauce\r\n1/2 cup	water\r\n1 - 8g sachet	MAGGI MAGIC SARAP\r\n', '1.	SautÃ© garlic in oil until golden brown.\r\n\r\n2.	Add sitaw and cook for 1minute.\r\n\r\n\r\n3.	Add vinegar, simmer for 10seconds then add water.\r\n\r\n4.	Bring to simmer and season with MAGGI MAGIC SARAP. Check seasoning.\r\n\r\n\r\n5.	Serve immediately.\r\n'),
(57, 'New Recipes 123', 'This is a cool description', 'Arroz-Ala-Cubana.png', 0, 0, 0, 0, 0, '2', 23, 123, 5553, 21, 12, 41, 'Test', 0, 0, 1, 0, 0, 0, 1, 0, 1, 'ffdadfdsaf', 'fdfsadff'),
(59, 'Edited Food', 'reqrqreq', 'Binagoongang-Lechon-Kawali-sa-Gata.png', 0, 0, 1, 0, 0, '1', 3, 2, 4, 2, 1, 4, '111', 0, 0, 1, 0, 0, 0, 0, 1, 0, '232332', '12313'),
(60, 'This is a super duper cool recipe', 'This is a super duper cool description', 'Bangus-Spring-rolls.png', 0, 1, 0, 0, 0, '2-3', 20, 30, 10, 100, 12, 50, '100 is the summary', 0, 1, 0, 0, 0, 0, 1, 0, 0, 'Cool Ingredients Bro', 'Yey'),
(61, 'Masayang recipe', 'This is cool dude', 'Adobong-Sitaw.png', 0, 0, 1, 0, 0, '1-2', 30, 40, 100, 250, 10, 30, 'This is a summary', 0, 0, 1, 0, 0, 0, 0, 1, 0, 'Cool Ingredients', 'THis is the cool directions'),
(62, 'This is a cool recipe', 'Super duper cool', 'Chopsuey.png', 0, 1, 0, 0, 1, '10-20', 45, 30, 200, 150, 200, 300, 'This is a cool summary', 1, 0, 0, 0, 1, 1, 0, 0, 1, '1\r\nB\r\nC', 'E\r\nF\r\nG'),
(63, 'Bangus Sinigang', 'Bangus Sinigang', 'Bangus-Sinigang.png', 0, 1, 0, 0, 0, '2-3', 20, 30, 100, 200, 300, 100, 'Cool Summary Bro', 1, 0, 0, 0, 0, 1, 0, 0, 0, '1 liter water\r\n2 pcs medium tomato, cut into quarters\r\n1 pc medium onion, cut into quarters\r\n2 pcs siling panigang\r\n1 pc medium bangus, cleaned and slice into 8 pieces\r\n1 pack 22g MAGGI MAGIC SINIGANGÂ® Original Sampalok Mix\r\n2 bundles talbos ng kamote, leaves picked\r\n2 bundles talbos ng sayote, leaves picked', '1. Combine water, tomatoes, onion and siling panigang in a pot. Bring to a boil and simmer for 15minutes.\r\n2. Add bangus and simmer for 10minutes. Skim the scum\r\n3. Pour MAGGI MAGIC SINIGANGÂ® Original Sampalok Mix.\r\n4. Stir in talbos ng kamote and talbos ng sayote and remove from heat. Serve immediately.'),
(64, '1', '1', 'Binagoongang-Lechon-Kawali-sa-Gata.png', 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '1 liter	water\r\n2 pcs	medium tomato, cut into quarters\r\n1 pc	medium onion, cut into quarters\r\n2 pcs	siling panigang\r\n1 pc	medium bangus, cleaned and slice into 8 pieces\r\n1 pack	22g MAGGI MAGIC SINIGANGÂ® Original Sampalok Mix\r\n2 bundles	talbos ng kamote, leaves picked\r\n2 bundles	talbos ng sayote, leaves picked\r\n', '1.	Combine water, tomatoes, onion and siling panigang in a pot. Bring to a boil and simmer for 15minutes.\r\n2.	Add bangus and simmer for 10minutes. Skim the scum\r\n3.	Pour MAGGI MAGIC SINIGANGÂ® Original Sampalok Mix.\r\n4.	Stir in talbos ng kamote and talbos ng sayote and remove from heat. Serve immediately.\r\n'),
(66, '11', '1111', '', 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '1 liter	water\r\n2 pcs	medium tomato, cut into quarters\r\n1 pc	medium onion, cut into quarters\r\n2 pcs	siling panigang\r\n1 pc	medium bangus, cleaned and slice into 8 pieces\r\n1 pack	22g MAGGI MAGIC SINIGANGÂ® Original Sampalok Mix\r\n2 bundles	talbos ng kamote, leaves picked\r\n2 bundles	talbos ng sayote, leaves picked\r\n', '1 liter	water\r\n2 pcs	medium tomato, cut into quarters\r\n1 pc	medium onion, cut into quarters\r\n2 pcs	siling panigang\r\n1 pc	medium bangus, cleaned and slice into 8 pieces\r\n1 pack	22g MAGGI MAGIC SINIGANGÂ® Original Sampalok Mix\r\n2 bundles	talbos ng kamote, leaves picked\r\n2 bundles	talbos ng sayote, leaves picked\r\n'),
(67, '1', '2212', '', 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '1 liter	water\r\n2 pcs	medium tomato, cut into quarters\r\n1 pc	medium onion, cut into quarters\r\n2 pcs	siling panigang\r\n1 pc	medium bangus, cleaned and slice into 8 pieces\r\n1 pack	22g MAGGI MAGIC SINIGANGÂ® Original Sampalok Mix\r\n2 bundles	talbos ng kamote, leaves picked\r\n2 bundles	talbos ng sayote, leaves picked\r\n', '1 liter	water\r\n2 pcs	medium tomato, cut into quarters\r\n1 pc	medium onion, cut into quarters\r\n2 pcs	siling panigang\r\n1 pc	medium bangus, cleaned and slice into 8 pieces\r\n1 pack	22g MAGGI MAGIC SINIGANGÂ® Original Sampalok Mix\r\n2 bundles	talbos ng kamote, leaves picked\r\n2 bundles	talbos ng sayote, leaves picked\r\n'),
(68, '1       ', '', '', 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '1 liter	water\r\n2 pcs	medium tomato, cut into quarters\r\n1 pc	medium onion, cut into quarters\r\n2 pcs	siling panigang\r\n1 pc	medium bangus, cleaned and slice into 8 pieces\r\n1 pack	22g MAGGI MAGIC SINIGANGÂ® Original Sampalok Mix\r\n2 bundles	talbos ng kamote, leaves picked\r\n2 bundles	talbos ng sayote, leaves picked\r\n', '1 liter	water\r\n2 pcs	medium tomato, cut into quarters\r\n1 pc	medium onion, cut into quarters\r\n2 pcs	siling panigang\r\n1 pc	medium bangus, cleaned and slice into 8 pieces\r\n1 pack	22g MAGGI MAGIC SINIGANGÂ® Original Sampalok Mix\r\n2 bundles	talbos ng kamote, leaves picked\r\n2 bundles	talbos ng sayote, leaves picked\r\n     '),
(69, '', '', '', 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos` (
`id` int(11) NOT NULL,
  `title` varchar(300) DEFAULT NULL,
  `caption` text,
  `videoName` varchar(500) DEFAULT NULL,
  `thumbnailName` varchar(500) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `videos`
--

TRUNCATE TABLE `videos`;
--
-- Dumping data for table `videos`
--

INSERT INTO `videos` VALUES
(6, '123', '2132131', '', 'Gulay.png'),
(8, 'This is a cool video', 'THis is a cool caption', 'Picturesque_30s_Final_Hires.mp4', 'Gulay.png'),
(9, 'Super Cool Video!!!', 'Dude the caption is cool and again', 'Picturesque_30s_Final_Hires (1).mp4', 'Adobong-Sitaw.png'),
(10, 'Cool Title 1', 'Cool Caption 1', 'Egg_Testimonials_-_JSM_30s_Final_Hires.mp4', 'Gulay.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `recipes`
--
ALTER TABLE `recipes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `recipes`
--
ALTER TABLE `recipes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
