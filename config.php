<?php
	//  Database Config File
	define('DB_NAME','maggie');
	define('DB_HOST','localhost');
	define('DB_USER','root');
	define('DB_PASS','root');

    define('IMAGE_BASE_URI', 'http://localhost:8888/maggie/uploads/images/');
    define('VIDEO_THUMBNAIL_BASE_URI', 'http://localhost:8888/maggie/uploads/videos/thumbnails/');
    define('VIDEO_BASE_URI', 'http://localhost:8888/maggie/uploads/videos/');
?>