<?php

    $table = $_GET['data'];

    if (!$table){
        $session_table_name = 'recipes';
    }else{
        $session_table_name = $table;
    }

    include_once( 'libs/getData.php' );

    if($data !== 0) {
        foreach($data as $key => &$value){
            if ($session_table_name == 'recipes'){
                $value["imageURI"] = IMAGE_BASE_URI.$value['imageName'];
                $ingredientsArray = explode("\r\n",$value['ingredients']);

                foreach ($ingredientsArray as $ingredient){
                    $value["ingredientsArr"][]['ingredient'] = $ingredient;
                }

                $directionsArray = explode("\r\n",$value['directions']);

                foreach ($directionsArray as $direction){
                    $value["directionsArr"][]['direction'] = $direction;
                }

                //unset unused fields
                unset($value['ingredients']);
                unset($value['directions']);
                unset($value['imageName']);
            }else {
                $value["thumbnailURI"] = VIDEO_THUMBNAIL_BASE_URI.$value['thumbnailName'];
                $value["videoURI"] = VIDEO_BASE_URI.$value['videoName'];

                unset($value['thumbnailName']);
                unset($value['videoName']);
            }
        }

        //print_r($data);
        print(json_encode($data));
    }else{
        echo "there is no data available";
    }
