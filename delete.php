<?php
    include_once( 'views/header.php' );
	include_once( 'libs/deleteData.php' );

    if(isset($success)){
        echo '<div class="alert alert-success"><p>'.$success.'</p></div>';
    }

    if(isset($error)){
        echo '<div class="alert alert-error"><p>'.$error.'</p></div>';
    }

?>
    <h2 class="left"> Delete an Item </h2>
    <span class="right"><a href="index.php" class="btn btn-success">Go Back</a></span>
    <div class="clear"></div>

<?php
    if (!isset($success) && (!isset($error))){
?>
        <div class="alert alert-info">
            <h3> Are you sure you want to delete data for <?php echo $_GET['itemName']; ?>?</h3>
            <a href="delete.php?id=<?php echo $_GET['id']; ?>&&delete=true" class="btn btn-danger">Yes Delete</a>
            <a href="index.php" class="btn"> Cancel </a>
        </div><!-- end alert -->
<?php
    }
?>

    </div><!-- end container -->
    </body>
</html>