<?php

$session_table_name = 'recipes';

if (isset($_POST['recipe_id'])){
    $param = $_POST;
    include_once( 'core/class.ManageDatabase.php' );
    $init = new ManageDatabase;

    $id = $param['recipe_id'];

    $data = $init->updateNumViews($session_table_name, $id);
    $data = $data[0];
    print(json_encode($data));
}